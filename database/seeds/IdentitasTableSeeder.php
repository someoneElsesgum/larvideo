<?php

use Illuminate\Database\Seeder;
use App\Identity;

class IdentitasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 10; $i++) {
          $r = Identity::create([
            'nama' => 'nama'.$i,
            'tempat_lhr' => 'Jakarta',
            'tgl_lhr' => today(),
            'alamat' => 'Bebas'
          ]);
        }
    }
}
