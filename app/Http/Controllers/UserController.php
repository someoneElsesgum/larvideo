<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Auth;
use Hash;

class UserController extends Controller
{
    public function index()
    {
      $us = DB::table('users')
              ->select('users.*','status_logins.user_id','status_logins.status')
              ->leftJoin('status_logins','status_logins.user_id','=','users.id')
              ->get();
      return view('user')->with(compact('us'));
    }

    public function add()
    {
      return view('crud.tambah_user');
    }

    public function pass()
    {
        $r = Auth::user();
        return view('ubahpass',compact('r'));
    }

    public function ubahPass(Request $request)
    {
        $aut = Auth::user();
        $passla = Hash::check($request->passla,$aut->password);
        $passba = Hash::make($request->passbar);
        $passb = $request->passbar;
        $konfi = $request->konpass;

        if ($passla != $aut->password) {
          return back()->with('pesan','Password Lama tidak sama');
        }
        if ($passb != $konfi)
        {
          return back()->with('pesan','Konfirmasi Password tidak sama');
        }
        $aut->update(['password'=>$passba]);
        return back()->with('pesanb','Ubah sukses');

    }

    public function store(Request $request)
    {
      $rules = [
          'nm' => 'required|string|max:255',
          'email' => 'required|string|email|max:255|unique:users',
          'pass' => 'required|string|min:6'
      ];

      $message = [
          'nm.required' => 'Tidak boleh kosong',
          'email.required' => 'Tidak boleh kosong',
          'pass.required' => 'Tidak boleh kosong',
          'pass.min' => 'Minimal harus 6 karakter'
      ];

      $this->validate($request, $rules, $message);

      $data = new User;
      $data->name = $request->nm;
      $data->email = $request->email;
      $data->password = Hash::make($request->pass);
      $data->save();

      return redirect('user')->with(['pesanb' => 'Berhasil disimpan']);
    }

    public function hapus($id)
    {
      $user = User::find($id);
      $user->delete();
      return back();
    }

    public function edit($id)
    {
      $us = User::find($id);
      return view('crud.edit_user')->with(compact('us'));
    }

    public function update(Request $request, $id)
    {
      $sim = User::find($id);
      $sim->name = $request->nm;
      $sim->email = $request->email;
      if($request->pass != ''){
        $sim->password = Hash::make($request->pass);
      }
      $sim->save();

      return redirect('user')->with('pesanb','Berhasil disimpan');

    }
}
