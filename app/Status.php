<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status_logins';
    protected $id = 'id';
    protected $guard = [];

    public function status()
    {
      return $this->belongsTo(User::class, 'user_id');
    }
}
