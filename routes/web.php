<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::group(['middleware' => 'auth'], function(){
  Route::get('/home', function () {
    return view('selamatdatang');
  });
  Route::get('/', 'HomeController@index')->name('home');
  Route::get('cctv','HomeController@cctv')->name('cctv');

  Route::group(['prefix' => 'user'],function(){
    Route::get('/','UserController@index')->name('us');
    Route::get('tambah','UserController@add')->name('add_us');
    Route::post('store','UserController@store')->name('store_us');
    Route::post('delete/{id}','UserController@hapus')->name('del_us');
    Route::get('edit/{id}','UserController@edit')->name('edit_us');
    Route::post('update/{id}','UserController@update')->name('update_us');
  });

  Route::get('ubahpass','UserController@pass')->name('pass');
  Route::post('savepass','UserController@ubahPass')->name('ubahpass');
});
