@extends('layouts.layout')

@section('title')
	Ubah Password
@endsection


@section('content')
<div class="container-fluid">
	<div class="row clearfix">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="card">
				<div class="header">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-12">
              <h2>Tambah Data</h2>
            </div>
          </div>
        </div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-12">
							<form action="{{ route('ubahpass') }}" method="POST" enctype="multipart/form-data">
								{{ csrf_field() }}
								<div class="col-sm-8">
									@if($m = Session::get('pesan'))
									<div class="alert alert-block alert-warning">
										<button type="button" class="close" data-dismiss="alert">
											<i class="ace-icon fa fa-times"></i>
										</button>

										<em class="fa fa-lg fa-warning">&nbsp;</em> {{ $m }}
									</div>
									@elseif($ms = Session::get('pesanb'))
									<div class="alert alert-block alert-success">
										<button type="button" class="close" data-dismiss="alert">
											<i class="ace-icon fa fa-times"></i>
										</button>

										<em class="fa fa-lg fa-thumbs-up">&nbsp;</em> {{ $ms }}
									</div>
									@endif
									<div class="form-group">
										<div class="form-line">
											<input type="password" name="passla" class="form-control" placeholder="Password Lama" required>
										</div>
									</div>
									<div class="form-group">
										<div class="form-line">
											<input type="password" name="passbar" class="form-control" placeholder="Password Baru" required>
										</div>
									</div>
									<div class="form-group">
										<div class="form-line">
											<input type="password" name="konpass" class="form-control" placeholder="Konfirmasi Password" required>
										</div>
									</div>
									<div>
										<button class="btn btn-primary waves-effect">Simpan</button>
										<button type="reset" class="btn btn-warning waves-effect">Reset</button>
									</div>
								</div>
								<div class="col-sm-3">
									<img src="{{ asset('gambar/profile-pic.jpg') }}" width="100%" class="thumbnail">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')

@endsection
