@extends('layouts.layout')

@section('title')
  Menu CCTV
@endsection

@section('content')
<div class="container-fluid">
  <!-- CPU Usage -->
  <div class="row clearfix">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      <div class="card">
        <div class="header">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-6">
              <h2>CCTV Gambar</h2>
            </div>
          </div>
        </div>
        <div class="body">
          <img src="gambar/cctv2.jpg" name="slide" width="400" height="300">
        </div>
      </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      <div class="card">
        <div class="header">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-6">
              <h2>CCTV Video</h2>
            </div>
          </div>
        </div>
        <div class="body">
          <div class="m-b-md" style="align-items: center;
                                      display: flex;
                                      justify-content: center;">
            <video id="my-video" class="video-js" controls preload="auto" width="400" height="300" poster="" data-setup="{}">
              <source src="gambar/v1.mp4" type='video/mp4'>
              <p class="vjs-no-js">
                To view this video please enable JavaScript, and consider upgrading to a web browser that
                <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
              </p>
              <div class="vjs-text-track-display"></div>
              <div class="vjs-loading-spinner"></div>
            </video>
            <!-- <div id="gambar"></div> -->
          </div>
        </div>
      </div>
    </div>
    </div>
    <!-- #END# CPU Usage -->
  </div>
@endsection

@section('js')
<script type="text/javascript">
      var image1 = new Image();
      image1.src = "gambar/cctv1.jpg";

      var image2 = new Image();
      image2.src = "gambar/cctv2.jpg";

      var image3 = new Image();
      image3.src = "gambar/cctv3.jpg";

      var image4 = new Image();
      image4.src = "gambar/cctv4.jpg";

      var image5 = new Image();
      image5.src = "gambar/cctv5.jpg";

      var image6 = new Image();
      image6.src = "gambar/cctv6.jpg";

      var image7 = new Image();
      image7.src = "gambar/cctv7.jpg";

      var image8 = new Image();
      image8.src = "gambar/cctv8.jpg";

      var image9 = new Image();
      image9.src = "gambar/cctv9.jpg";

      var image10 = new Image();
      image10.src = "gambar/cctv10.jpg";

      var step=1;
      function gerak(){
        if(!document.images) {
            return
        } else {
            document.images.slide.src = eval("image"+step+".src");
        }

        if(step < 10)
            step++;
        else
            step=1;
            setTimeout("gerak()",1000);
      }
      gerak();

      videojs("#my-video").ready(function(event) {
          var myPlayer = this;
          var myPlayer2 = this;
          myPlayer.play();
      });
</script>
@endsection
