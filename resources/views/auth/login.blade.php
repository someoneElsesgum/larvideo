@extends('layouts.first')

@section('title')
  Sign In
@endsection

@section('content')
  <div class="body">
      <form id="sign_in" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
          <div class="msg">Sign in to start your session</div>
          <div class="input-group">
              <span class="input-group-addon">
                  <i class="material-icons">mail</i>
              </span>
              <div class="form-line">
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
              </div>
          </div>
          @if ($errors->has('email'))
          <span class="help-block">
            {{ $errors->first('email') }}
          </span>
          @endif
          <div class="input-group">
              <span class="input-group-addon">
                  <i class="material-icons">lock</i>
              </span>
              <div class="form-line">
                  <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
              </div>
          </div>
          <div class="row">
              <div class="col-xs-8 p-t-5">
                  <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="remember" class="filled-in chk-col-pink">
                  <label for="remember">Remember Me</label>
              </div>
              <div class="col-xs-4">
                  <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
              </div>
          </div>
          <div class="row m-t-15 m-b--20">
              <div class="col-xs-6">
                  <a href="{{ route('register') }}">Register Now!</a>
              </div>
              <div class="col-xs-6 align-right">
                  <a href="{{ route('password.request') }}">Forgot Password?</a>
              </div>
          </div>
      </form>
  </div>
@endsection

@section('js')
  <script src="{{ asset('js/examples/sign-in.js') }}"></script>
@endsection
