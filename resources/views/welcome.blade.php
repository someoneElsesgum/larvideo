<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <link href="https://vjs.zencdn.net/7.4.1/video-js.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">

        <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
        <script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <video id="my-video" class="video-js" controls preload="auto" width="400" height="264" poster="" data-setup="{}">
                      <source src="gambar/v1.mp4" type='video/mp4'>
                      <p class="vjs-no-js">
                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                        <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                      </p>
                      <div class="vjs-text-track-display"></div>
                      <div class="vjs-loading-spinner"></div>
                    </video>
                    <!-- <img src="{{ asset('gambar/1.jpg') }}" width="640" height="264"> -->
                    <div id="gambar"></div>
                </div>

                <div class="cam-viewer-action">
                    <div class="btn-group">
                        <a href="#" id="refresh" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Refresh</a>
                        <a href="#" class="btv btn btn-default btn-sm cam-refresh-video"><i class="fa fa-youtube-play"></i> Video</a>
                        <a href="#" class="bti btn btn-default btn-sm cam-refresh-image"><i class="fa fa-picture-o"></i> Image</a>
                        <a href="#" class="btn btn-default btn-sm"><i class="fa fa-bar-chart"></i> Statistic</a>
                        <div class="btnm">
                        <a href="#" class="bts btn btn-default btn-sm"><i class="fa fa-volume-off"></i> Mute</a>
                        <a href="#" class="btd btn btn-default btn-sm"><i class="fa fa-volume-up"></i> Unmute</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://vjs.zencdn.net/7.4.1/video.js"></script>
        <script type="text/javascript">
            
            $(document).ready(function(){

              $("#my-video").hide();
              $(".bti").hide();
              $(".btd").hide();
              $(".bts").hide();

              myfungsi();

              function myfungsi(){
                var myImage = new Image(400, 264);
                myImage.src = 'gambar/1.jpg';
                x = document.getElementById("gambar");

                setTimeout(function(){ 
                x.appendChild(myImage); 
                }, 500);

              }


              $("#refresh").click(function(){
                videojs("#my-video").ready(function(event) {
                   var myPlayer = this;
                   var myPlayer2 = this;
                   myPlayer.load();
                   myPlayer2.play();
                });
              });

              $(".btv").click(function(){
                $("#my-video").show();
                $("img").hide();
                $(".btv").hide();
                $(".bti").show();
                videojs("#my-video").ready(function(event){
                   var myPlayer = this;
                   myPlayer.play();
                });
                $(".btnm").show();
              });

              $(".bti").click(function(){
                $("img").show();
                $("#my-video").hide();
                $(".bti").hide();
                $(".btv").show();
                $(".btnm").hide();
              });

              $(".bts").click(function(){
                videojs("#my-video").ready(function(event){
                   var myPlayer = this;
                   var myPlayer2 = this;
                   myPlayer.muted(true);
                   $(".bts").hide();
                   $(".btd").show();
                });
              });

              $(".btd").click(function(){
                videojs("#my-video").ready(function(event){
                   var myPlayer = this;
                   var myPlayer2 = this;
                   myPlayer.muted(false);
                   $(".btd").hide();
                   $(".bts").show();
                });
              });

            });
        </script>
    </body>
</html>